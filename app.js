var path = require("path");
var express = require('express');
var app = express();
// creating the peerjs server
var ExpressPeerServer = require('peer').ExpressPeerServer;


// determining the port
var port = Number(process.env.PORT || '3000');


app.use(express.static('public'));


// routes
var home = require('./routes/index');
app.use('/', home);

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');



var server = app.listen(port, function() {
    console.log('Server is running on port ' + port);
});

// peerjs server setup
var peerServer = ExpressPeerServer(server);
app.use('/app', peerServer);
peerServer.on('connection', function(id) { console.log(id); });


console.log(port);
