// globals
//----------------------------
var player1;
var player2;
var conn;
var host = "herokuapp.com";

var board = [];
var restartbtn = false;
var winningcombo = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6]
];
turn = getRandomInt(0, 1);

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}
//----------------------------


// peer connection to the server
//----------------------------

var peer = new Peer({host: '/', port: '', path: '/app'});
//----------------------------


// recieve connections
//----------------------------
peer.on('connection', connect);


// logging our ID to the console
//----------------------------
peer.on('open', function(id) {
    console.log('My peer ID is: ' + id);
    $('#pid').html(id)
});



// TODO **********************
//----------------------------
//console.log(peer.connections);




// DOM stuff
//----------------------------
$(document).ready(function() {

    $('#start').click(function(e) {
        e.preventDefault();
        if (validateName()) {
            var plyrName = $('#plyrNAME').val();
            createCrossPlayer(plyrName);
            $('.hero-content-hidden-start').fadeIn().css('display', 'flex');
        }

    });
    $('#join').click(function(e) {
        e.preventDefault();
        if (validateName()) {
            var plyrName = $('#plyrNAME').val();
            createCirclePlayer(plyrName)
            $('.hero-content-hidden-join').fadeIn().css('display', 'flex');
        }

    });
    $('form#plyrIDForm').submit(function(e) {
        e.preventDefault();
        var friendID = $('#plyrID').val();

        createConnection(friendID);

    });
    $('.block').click(function(e) {

        // console.log(e)

        var that = this;
        var move = $(this).data('num');
        makeMove(move, that);
    })
    $('#restartGame').click(function() {
        restartbtn = true;

        $(this).html('Waiting for your opponent...')
        conn.send({ team: player1.team, restart: true });
        $(this).addClass('no-click')
        checkRestart();
        //prepare();
    });
    $('form#messageForm').submit(function(e) {
        e.preventDefault();
        var message = $('#message').val();
        $('#message').val('');
        $('.messages').append("<div class='message1'>@" + player1.name + ": " + message + "</div>");
        scrollDown()
        conn.send({ message: true, body: message });
    })
});
//----------------------------

function scrollDown(){
    var elem = $(".messages")[0];
        elem.scrollTop = elem.scrollHeight;
}
// Creating player1
//----------------------------
function createCrossPlayer(plyrName) {
    player1 = new Player1(plyrName, 'cross')
}

function createCirclePlayer(plyrName) {
    player1 = new Player1(plyrName, 'circle')
}


// Creating the connection to the peer
//----------------------------
function createConnection(friendID) {
    $('#playrJoinBtn').addClass('no-click').val('Connecting...')
    var con = peer.connect(friendID, { metadata: { name: player1.name, team: player1.team, turn: turn } });
    con.on('open', function() {
        connect(con);
        con.send({ metadata: true });
    });
}

// player object constructors
//----------------------------
function Player1(name, team) {
    this.name = name;
    this.team = team;
}
Player1.prototype.turn = false;
Player1.prototype.moves = [];
Player1.prototype.won = false;
Player1.prototype.score = 0;

function Player2(name, team) {
    this.name = name;
    this.team = team;
}

Player2.prototype.turn = false;
Player2.prototype.moves = [];
Player2.prototype.won = false;
Player2.prototype.score = 0;

var restart = 0;

function connect(c) {

    conn = c;
    conn.on('data', function(data) {
        if (data.metadata == true) {
            createPlayer2();
            conn.send({ metadata: 'back', name: player1.name, team: player1.team });

            prepare();

        }
        if (data.metadata == 'back') {
            // this data came back! horray, lets make a player2
            player2 = new Player2(data.name, data.team);

            if (turn == 0) {
                player2.turn = true;
                player1.turn = false;
            } else {
                player2.turn = false;
                player1.turn = true;
            }

            prepare();
        }
        if (data.move) {
            animateMove2(data);
        }


        if (data.restart) {
            restart++;
        }
        if (data.krdostart) {
            restart = 0;
            prepare();
        }
        if (data.message) {
            printMessage(data.body);
        }
    });

    conn.on('close', function(){
        alert(player2.name + " has left the game!");
    })


}

function checkRestart() {
    if (restart == 1) {
        restart = 0;
        conn.send({ krdostart: true })
        prepare();
    }
}


//
// call the prepare method when you want something to happen on either side
//


//----------------------------
function createPlayer2() {
    var data = conn.metadata;
    // creating the other player

    player2 = new Player2(data.name, data.team)
    if (conn.metadata.turn == 0) {
        player1.turn = true;
        player2.turn = false;
    } else {
        player1.turn = false;
        player2.turn = true;
    }
}

function prepare() {
    // initialize board
    initBoard();
    turnSelect();
    animateTurn();
    initScore();
    prepareCanvas();
    $('#inGame').fadeOut();
    $('#hero').fadeOut()
}

function initScore() {
    $('#score1').html(player1.score);
    $('#score2').html(player2.score);
}

function turnSelect() {
    if (player1.won) {
        player1.won = false;
        player1.turn = true;
        player2.turn = false;
    }
    if (player2.won) {
        player2.won = false;
        player1.turn = false;
        player2.turn = true;
    }
}

function clearBoard() {
    $('.block').css('background', '');
}

function prepareCanvas() {
    clearBoard();
    $('#restartGame').html('Restart')
    $('#playr1').html(player1.name)
    $('#playr2').html(player2.name)
    $('#restartGame').removeClass('no-click')
    $('.header').slideDown();
    $('#chat').fadeIn();
}
//----------------------------




//
// game functions
//----------------------------
function printMessage(message) {
    $('.messages').append("<div class='message2'>@" + player2.name + ": " + message + "</div>");
scrollDown()
}

function initBoard() {
    player1.moves = []
    player2.moves = []
    board = [];
    for (var i = 0; i < 3; i++) {
        board.push([0, 0, 0])
    }
}

function makeMove(move, that) {
    if (player1.turn) {
        animateMove(move, that);
        sendMove(move)
    } else {
        alert('its not turn yet!')
    }
}

function sendMove(move) {
    conn.send({ move: true, action: move, team: player1.team })
}

function animateMove(move, block) {
    if (player2.moves.indexOf(move) == -1) {
        if (player1.moves.indexOf(move) == -1) {

            player1.moves.push(move);

            if (player1.turn) {
                player1.turn = false;
                player2.turn = true;
            }


            var temp = player1.team;
            var image = 'url(/images/' + temp + '.svg)no-repeat center center';
            $(block).css('background', image);

            // todo

            if (checkVictory(player1) == undefined) {
                animateTurn();
            }

        }
    } else {
        alert('This move has already been made');
    }
}

function animateMove2(data) {
    if (data.move == true) {
        if (player1.moves.indexOf(data.action) === -1) {

            if (player2.moves.indexOf(data.action) == -1) {
                player2.moves.push(data.action);
                player1.turn = true;
                player2.turn = false;

                var action = data.action;
                var block = ".block[data-num=" + action + "]";
                var temp = player2.team;
                var image = 'url(/images/' + temp + '.svg)no-repeat center center';

                $(block).css('background', image);

                if (checkVictory(player2) == undefined) {
                    animateTurn();
                }
            }
        }
    }
}

function checkVictory(player) {
    var iswon = false;
    for (var i = 0; i < winningcombo.length; i++) {
        var diff = _.intersection(player.moves, winningcombo[i]);
        if (_.isEqual(diff.sort(), winningcombo[i])) {

            iswon = true;
            won(player);
            player.score++;
            player.won = true;
            return true;

            // won();
            break;
        }

    }

    if (!iswon) {
        checkDraw();
    }

}

function checkDraw(){
  var range = _.range(0, 9);
  var totalMoves = _.union(player1.moves, player2.moves).sort();
  if (_.isEqual(range, totalMoves)){
    draw();
  }
}

function animateTurn() {
    if (player1.turn) {
        $('#playr1').removeClass('disabled').addClass('animated flash')
        $('#playr2').removeClass('animated flash').addClass('disabled');
    }
    if (player2.turn) {
        $('#playr2').removeClass('disabled').addClass('animated flash')
        $('#playr1').removeClass('animated flash').addClass('disabled');
    }
}

function won(player) {
    if (player1.won) {
        player1.won = true;
        player2.won = false;
    }
    if (player2.won) {
        player2.won = true;
        player1.won = false;
    }

    $('#topText').html(player.name + " won!");
    $('#inGame').fadeIn()

}

function draw() {
    $('#topText').html("Draw!");
    $('#bottomText').html("keep playing!");
    $('#inGame').fadeIn()
}

function arraysEqual(arr1, arr2) {
    if (arr1.length !== arr2.length)
        return false;
    for (var i = arr1.length; i--;) {
        if (arr1[i] !== arr2[i])
            return false;
    }

    return true;
}

//----------------------------




// validations
//----------------------------
function validateName() {
    var plyrName = $('#plyrNAME').val();
    if (plyrName.length <= 0) {
        alert('Please enter a name');
        return false;
    } else {
        return true;
    }
}


// error handling
// elementary error handling
peer.on('error', function(err){
    if(err.type == 'peer-unavailable'){
        $('#playrJoinBtn').removeClass('no-click').val('Join');
        alert('Your friend could not be found');
    }

    if(err.type == 'browser-incompatible'){
        alert('Your browser does not support this game! Sorry!');
    }


    if(err.type == 'webrtc'){
        alert('Something terrible happened. Contact @candhforlife');
    }
});
